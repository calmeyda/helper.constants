const setValues = require('./setValues');

module.exports = (constants) => {
  return {
    getAll: () => constants,
    getValues: () => Object.keys(constants).map((key) => constants[key].value),
    getString: () =>
      Object.keys(constants)
        .map((key) => constants[key].value)
        .join(','),
    getLabel: (key) => constants[key].label,
    ...setValues(constants),
  };
};
